import os

def init_clips():
    file_map = {}
    files = os.listdir('./audio_clips')
    for f in files:
        if os.path.isfile(f"./audio_clips/{f}"):
            fname = f.split('.')[0]
            file_map[f">{fname}"] = f
        else:
            temp = []
            clips = os.listdir(f'./audio_clips/{f}')
            for clip in clips:
                temp.append(clip)
            file_map[f">{f}"] = temp
                
    return file_map
