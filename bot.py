import discord
import requests
import praw
import os
import time
import random
import asyncio
from discord import Embed
from helpers import init_clips

reddit = praw.Reddit(client_id=os.getenv("REDDIT_CLIENT_ID"),
                     client_secret=os.getenv("REDDIT_CLIENT_SECRET"),
                     password=os.getenv("REDDIT_PASSWORD"),
                     user_agent=os.getenv("REDDIT_USER_AGENT"),
                     username=os.getenv("REDDIT_USERNAME"))

class DiscordBot(discord.Client):
    def __init__(self):
        self.billy_mays_here = None
        self.bot_admin = None
        self.clips = init_clips()
        self.voice_client = None
        self.queue = []
        self.enable_join_audio = False if int(os.getenv("JOIN_AUDIO")) == 0 else True
        self.join_clip = "join.mp3"
        discord.Client.__init__(self)

        self.bg_task = self.loop.create_task(self.check_if_empty())

    async def on_ready(self):
        print('Logged in as {0.user}'.format(client))
        self.billy_mays_here = self.get_guild(109551664364548096)
        self.bot_admin = self.billy_mays_here.get_role(747233126077562922)

    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if message.content.startswith('|reddit'):
            subreddit = message.content.split(' ')[1]
            if reddit.subreddit(subreddit).over18:
                await self.play_clip('audio_clips/fbi.mp3', message.author.voice.channel)
            else:
                await message.channel.send(reddit.subreddit(subreddit).random().url)

        if message.content.startswith('|clips'):
            clips = ""
            offset = len(max(self.clips.keys(), key=len))
            for index, clip in enumerate(self.clips.keys()):
                if (index+1) % 5 == 0:
                    clips += f"{clip}\n"
                else:
                    clips += clip + (' ' * (offset - len(clip))) + ' ' * 5
            clips = f"```{clips}```"
            await message.channel.send(clips, delete_after=30)

        if message.content.startswith('|clear'):
            def is_command(m):
                return m.content.startswith('|') or m.content.startswith('>')
            
            deleted = await message.channel.purge(limit=10, check=is_command)
        
        if message.content.startswith('|mall'):
            if self.bot_admin in message.author.roles:
                members = message.author.voice.channel.members
                for member in members:
                    await member.edit(mute=True)
                
        if message.content.startswith('|umall'):
            if self.bot_admin in message.author.roles:
                members = message.author.voice.channel.members
                for member in members:
                    await member.edit(mute=False)
                
        if message.content.startswith('|upload'):
            if self.bot_admin in message.author.roles:
                for attachment in message.attachments:
                    print(attachment.filename)
                    name, extension = attachment.filename.split('.')
                    if extension != "mp3":
                        await message.channel.send("Sorry only .mp3 files are accepted", delete_after=30)
                    else:
                        if name in tuple(self.clips.keys()):
                            await message.channel.send("A clip with that name already exists, pls rename the file", delete_after=30)
                        else:
                            self.clips[f">{name}"] = attachment.filename
                            await attachment.save(f"/app/audio_clips/{attachment.filename}")

        if message.content.startswith('|enable_join_audio'):
            if self.bot_admin in message.author.roles:
                if self.enable_join_audio:
                    self.enable_join_audio = False
                else:
                    self.enable_join_audio = True
        
        if message.content.startswith('|join_clip'):
            if self.bot_admin in message.author.roles:
                if len(message.content.split(' ')) <= 1:
                    await message.channel.send("Please provide a clip to set as join message(hint: use |clips to see all clips)", delete_after=30)
                else:
                    if message.content.split(' ')[1] in self.clips.keys():
                        self.join_clip = self.clips[message.content.split(' ')[1]]
                    else:
                        await message.channel.send("Not a valid clip(hit: use |clips to see all clips)", delete_after=30)

        if message.content.startswith(tuple(self.clips.keys())):
            if isinstance(self.clips[message.content], list):
                await self.play_clip(f'audio_clips/{message.content[1:]}/{random.choice(self.clips[message.content])}', message.author.voice.channel)
            else:
                await self.play_clip(f'audio_clips/{self.clips[message.content]}', message.author.voice.channel)

        if message.content.startswith(('|', '>')):
            await message.delete()

    async def on_raw_reaction_add(self, payload):
        if str(payload.emoji) == '👎':
            message = await self.get_channel(payload.channel_id).fetch_message(payload.message_id)
            if message.author == self.user:
                await message.delete()

    async def on_voice_state_update(self, member, before, after):
        if member == self.user:
            return

        if after.channel == None:
            if len(self.voice_clients) > 0:
                if len(before.channel.members) == 1 and before.channel.id == self.voice_clients[0].channel.id:
                    await self.disconnect(self.voice_client)

        if self.enable_join_audio:
            if before.channel == None:
                if member.id == 122445354040819713:
                    await self.play_clip('audio_clips/cheese.mp3', after.channel, sleep=1)
                elif member.id == 107193589729087488:
                    await self.play_clip('audio_clips/wassup.mp3', after.channel, sleep=1)
                elif member.id == 109902776032575488:
                    await self.play_clip(f'audio_clips/kevin/{random.choice(self.clips[">kevin"])}', after.channel, sleep=1)
                else:
                    await self.play_clip(f'audio_clips/{self.join_clip}', after.channel, sleep=1)

    async def disconnect(self, v_client):
        while v_client.is_playing():
            None
        await v_client.disconnect()

    async def play_clip(self, clip, channel, sleep=None):
        loop = asyncio.get_event_loop()
        if len(self.voice_clients) == 0:
            self.voice_client = await channel.connect()

        if sleep:
            time.sleep(sleep)

        if self.voice_client.is_playing():
            self.queue.append(clip)
        else:
            source = await discord.FFmpegOpusAudio.from_probe(clip)
            self.voice_client.play(source, after=lambda e: loop.create_task(self.check_queue(channel)))
    
    async def check_queue(self, channel):
        if len(self.queue) > 0:
            next_clip = self.queue.pop(0)
            await self.play_clip(next_clip, channel)
        else:
            print("No more clips in queue")

    
    async def check_if_empty(self):
        while True:
            if len(self.voice_clients) > 0:
                print(len(self.voice_client.channel.members))
                print(self.voice_client.channel.id)
                if len(self.voice_client.channel.members) == 1:
                    print("Nobody in channel disconnecting")
                    await self.voice_client.disconnect()
            await asyncio.sleep(30)

if __name__ == "__main__":
    try:
        client = DiscordBot()
        client.run(os.getenv("DISCORD_BOT_SECRET"))
    except KeyboardInterrupt as e:
        print("Shutting down")
