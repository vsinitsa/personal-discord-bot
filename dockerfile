FROM python:3.7

ENV REDDIT_CLIENT_ID default
ENV REDDIT_CLIENT_SECRET default
ENV REDDIT_PASSWORD default
ENV REDDIT_USER_AGENT default
ENV REDDIT_USERNAME default

ENV DISCORD_BOT_SECRET default

ENV JOIN_AUDIO 1

WORKDIR /app
# COPY ./audio_clips ./audio_clips
COPY __init__.py .
COPY bot.py .
COPY helpers.py .
COPY requirements.txt .

RUN apt update
RUN apt install -y ffmpeg libffi-dev libnacl-dev python3-dev
RUN pip3 install -r requirements.txt

CMD python3 -u bot.py

